# Examen outils informatiques / Formation de base en informatique

**Note**: lorsque la mention "ajouter ce fichier au dépôt" ou "ajouter les chagements au dépôt" apparait il s'agit de créer un commit contenant le fichier ou les modifications apportées. Le commit devra porter comme nom "*SECT*_Q*X*" pour la question *X* de la section *SECT* (par exemple GIT_Q2). Si une question n'a pas de numéro simplement indiquer la section (e.g. SSH).

## A faire tous ensemble

1. *Forker* ce dépôt pour obtenir votre propre version sur gitlab.
2. *Cloner* votre version du dépôt (attention à bien utiliser la version SSH et PAS https).

## GIT (partie 1)

1. Créer une nouvelle branche portant votre nom.
2. Pour le reste de l'examen vous travaillerez sur cette nouvelle branche uniquement, sauf pour les toutes dernières questions (c.f. question *merge*).
3. Taper la commande `./script.sh` dans la racine du dépôt pour executer le script. Ce script a créé un fichier, ajouter ce fichier au dépôt.

## Shell

1. Supprimer **TOUS** les fichiers se terminant par \*.txt dans le dossier `txt`. Ajouter les changements au dépôt.
2. Supprimer le fichier `toerease` du dossier `damn`. Ajouter les changements au dépôt.
3. Le fichier `data.csv` contient des données au fomat colonne. Visualiser les 5 premières lignes de ce fichier et les stocker dans un fichier nommé `data-excerpt.csv`. Ajouter ce fichier au dépôt.
4. En combinant les commandes vues en cours ainsi que la commande `cut` (c.f. man), trouver une commande (i.e. une ligne) permettant d'obtenir *uniquement* le poucentage d'utilisation de la lettre "X". Ecrire cette commande dans un fichier nommé `extract-line.sh` et ajouter ce fichier à la racine du dépôt.

## SSH

En se connectant sur la machine [cuilxa.unige.ch](cuilxa.unige.ch) par SSH copier le fichier distant `/usr/local/chanel/toget` à la racine de votre dépôt local. Ajouter ce fichier au dépôt.

## Fichiers

En utilisant un éditeur hexadecimal, stocker dans un fichier nommé **data.bin** la valeur décimale 256 en *little-endian* sur deux octets. Ajouter ce fichier à la racine du dépôt.

## Swift et débuguage

1. Complétez le code de la fonction `isPalindrome` dans le fichier `code/Palindrome.swift`, laquelle retourne `true` si son argument est un palindrome. Pour rappel, un palindrome est une séquence de caractères qui peut se lire indifférement de gauche à droite ou de droite à gauche.
2. A l'aide d'un debugger, identifiez et corrigez le bug qui s'est glissé dans la fonction `primes` dans le fichier `Numbers.swift`, laquelle est supposée retourner la liste des nombres premiers contenus dans le tableau reçu argument.

## GIT (partie 2)

4. Effectuer un *merge* de la branche portant votre nom vers la branche master.
5. Monter tout le contenu local de votre dépôt (i.e. toutes les branches) vers votre *remote*.

# Soumettre l'évaluation

Se rendre sur moodle et soumettre l'adresse gitlab de votre dépôt dans le formulaire de l'évaluation.
